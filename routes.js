//// CREATE A ROUTE
// "/greeting"
const http = require("http");
let url = require("url");

const port = 4000;

const server = http.createServer((request, response) => {
	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Hello again.');
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Welcome.');
	} else {
		response.writeHead(400, {'Content-Type' : 'text/plain'});
		response.end('Page not available.')
	}
});

server.listen(port);
console.log(`Server is running at localhost: ${port}`);