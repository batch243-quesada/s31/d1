// Use the 'require' directive to load the http modeule of node JS
// A 'module' is software component or part of a program that contains one or more routines
// The 'http module' lets Node.js transfer data using the HTTP or the HyperText Transfer Protocol
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and Servers(nodeJS/ExpressJS applications) communicate by exchanging individual messages

// REQUEST - messages sent by the client (usually in a Web browser)
// RESPONSES - messages sent by the server as an answer to the client

let http = require("http");

//// CREATE A SERVER
// The http module has a createServer() method that accepts a function as an argument and allows server creation
// The arguments passed in the function are request & response objects (data type) that contain methods that allow us to receive requests from the clien and send the responses back
// Using the module's createServer() method, we can create an HTTP server that listens to the request on a specified port and gives back to the client
// http.createServer(function(request, response){})

// Define the Port nummber that the server will be listening to
// http.createServer(function(request, response){}.listen(4000))

// A port is virtual point where network connections start and end
// Send a response back to the client
// Use the writeHead() method 
// Set a status code for the response 
http.createServer(function(request, response){
	response.writeHead(200, {'Content-Type' : 'text/plain'});
	response.end('Hello, Worlds');
}).listen(4000);

console.log('Server running at localhost:4000');

// Inputting the command tells our device to run node js
// node index.js

// Install nidemon
// npm install -g nodemon

// To check if nodemon is already installed
// nodemon -v

//// IMPORTANT
// Installing the package will allow the server to automatically restart when files have been changed or updated
// -g refers to a global installation where the current version of the package dependency will be installed locally on our device allowing us to use nodemon on any project
